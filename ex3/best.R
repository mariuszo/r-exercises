# Write a function called best that take two arguments: the 2-character abbreviated name of a state and an
## outcome name. The function reads the outcome-of-care-measures.csv le and returns a character vector
## with the name of the hospital that has the best (i.e. lowest) 30-day mortality for the specied outcome
## in that state.

best <- function(state, outcome) {
  data <- read.csv("data/outcome-of-care-measures.csv");
  
  # validate state
  validStates <- unique(data$State)
  if(!state %in% validStates) {
    stop("invalid state")
  }
  
  # validate outcome
  validOutcomes <- c("heart attack", "heart failure", "pneumonia")
  if(!outcome %in% validOutcomes) {
    stop("invalid outcome")
  }
  
  # Assign column indices to outcome classes
  outcomes <- c(11, 17, 23)
  names(outcomes) <- validOutcomes
  
  # Retrieve data for particular state
  stateData <- data[data$State == state, ]
  
  # Get the column of selected state/metric
  selectedOutcomes <- as.numeric(as.character(stateData[, outcomes[[outcome]]]))
  
  # Get hospital names for selected state
  hospitalNames <- as.character(stateData[, 2])
  
  # Get a list of indices for state data, ordered first by metric, then by hospital name
  ordered <- order(selectedOutcomes, hospitalNames, na.last = T)
  
  # Get the entry from the state data, corresponding to top 1 index in ordered index list, 
  # get the hospital name and coerce it to character
  as.character(stateData[ordered[1],2][[1]])
}

setwd("c:/work/r/ex3")
x <- best("MD", "heart attack")